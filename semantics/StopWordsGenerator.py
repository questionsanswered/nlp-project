from nltk.corpus import stopwords

outFile = "StopWords.txt"
target = open(outFile, 'w')
stopWordSet = stopwords.words('english')
for each_word in stopWordSet:
    target.write(each_word +'\n')
target.close()
