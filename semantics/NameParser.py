import argparse
import csv

femaleNames = set()
maleNames = set()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("numNames")
    parser.add_argument("nameFile")
    args = parser.parse_args()
    numNames = args.numNames
    nameFile = args.nameFile
    text = open(nameFile)
    femaleCount = 0
    maleCount = 0
    for each_line in text:
        lineElem = each_line.split(",")
        if (lineElem[1] == 'F' and femaleCount < 1000):
            femaleNames.add(lineElem[0])
            femaleCount += 1
        elif (lineElem[1] == 'M' and maleCount < 1000):
            maleNames.add(lineElem[0])
            maleCount += 1
        if (femaleCount >= 1000 and maleCount >= 1000):
            break
    with open('HUMAN/names.txt', 'wb') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for each_female in femaleNames:
            spamwriter.writerow([each_female])
        for each_male in maleNames:
            spamwriter.writerow([each_male])



if __name__ == "__main__":
    main()
