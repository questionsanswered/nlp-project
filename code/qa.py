import argparse
import operator
import nltk
from nltk import word_tokenize
from nltk.stem.lancaster import LancasterStemmer
from nltk.tokenize import sent_tokenize
import string
import collections


questionDict = {}
storyDict = {}
storyID = []
titleSet = set()
humanSet = set()
nameSet = set()
occupationSet = set()
locationSet = set()
monthSet = set()
timeSet = set()
stopWordSet = set()
locPrepSet = set()
locNounSet = set()
punctuationSet = set(string.punctuation)
answerDict = {}

#adding some double quotes
punctuationSet.add('``')
punctuationSet.add('""')
punctuationSet.add("''")
punctuationSet.add('...')
punctuationSet.add('--')
punctuationSet.add("'s")
punctuationSet.add("n't")




def getListofNounPhrases(sentence, nounSet):
    currentPhrase = ''
    npList = list()
    for each_word in sentence:
        if each_word.lower() in stopWordSet or each_word.lstrip()[0].islower() or each_word in punctuationSet:
            if not currentPhrase == '':
                npList.append(currentPhrase.rstrip())
                currentPhrase = ''
        else:
            currentPhrase += each_word.lstrip().rstrip() + " "
    npList.append(currentPhrase.rstrip())
    npNounTaggedList = list()
    anyFound = False
    #determine whether the NP contains human word or not
    for each_np in npList:
        npArr = word_tokenize(each_np)
        nounTagged = False
        for each_word in npArr:
            if (each_word in nounSet or each_word.lower() in nounSet):
                nounTagged = True
                anyFound = True
                break
        npNounTaggedList.append((each_np, nounTagged))
    return npNounTaggedList, anyFound






#this function is used to calculate the score of each sentence with respect to Question
def WordMatch(questionWords, sentenceWords):

    lancaster_stemmer = LancasterStemmer()

    wordWeight = dict()

    #Remove stop words from sentence and calculate weight
    sentenceWords = [eachWord.lower() for eachWord in sentenceWords]

    taggedSW = nltk.pos_tag(sentenceWords)
    #print "after"
    #print taggedSW

    taggedWords = {}
    for ts in taggedSW:
        taggedWords[ts[0]] = ts[1]
        #print ts,'\n'

    #print taggedWords


    for each in sentenceWords:
         if each in stopWordSet or each in punctuationSet:
             continue
         tag = taggedWords[each]
         if 'VB' in tag:
            wordWeight[lancaster_stemmer.stem(each)] = 7
         else:
            wordWeight[lancaster_stemmer.stem(each)] = 3

    sentenceMorphed = [lancaster_stemmer.stem(eachWord) for eachWord in sentenceWords]
    questionMorphed = [lancaster_stemmer.stem(eachWord.lower()) for eachWord in questionWords]

    sentenceMorphed = set(sentenceMorphed)
    questionMorphed = set(questionMorphed)
    commonWords = questionMorphed.intersection(sentenceMorphed)

    score = 0
    for each in commonWords:
        if each in wordWeight:
            score += wordWeight[each]

    return score



def formatStory(storyFile):
    storyText = open(storyFile)
    textReached = False
    textAcc = ""
    valueDict = {}
    storyID = ""
    for each_line in storyText:
        if textReached:
            textAcc += each_line.rstrip().lstrip()+" "
            continue
        each_line_str = str(each_line)
        if each_line_str.startswith(HEADLINE):
            headline = each_line_str[each_line_str.index(':'):]
            valueDict[HEADLINE_KEY] = headline[1:].rstrip().lstrip()
        elif each_line_str.startswith(DATE):
            date = each_line_str[each_line_str.index(':'):]
            valueDict[DATE_KEY] = date[1:].rstrip().lstrip()
        elif each_line_str.startswith(STORY_ID):
            storyID = each_line_str[each_line_str.index(':')+1:].rstrip().lstrip()
        elif each_line_str.startswith(TEXT):
            textReached = True

    # titles are getting split too
    valueDict[TEXT_KEY] = sent_tokenize(textAcc.rstrip().lstrip())

    #for each sentence in the list strip spaces from the ends
    for i in range(0, len(valueDict[TEXT_KEY])):
        valueDict[TEXT_KEY][i] = valueDict[TEXT_KEY][i].rstrip().lstrip()

    storyDict[storyID] = valueDict


def formatQuestionFile(questionFile, storyID):
    questionText = open(questionFile)
    currentQuestionID = ""
    currentQuestion = ""
    questionDict[storyID] = list()
    for each_line in questionText:
        each_line_str = str(each_line)
        if each_line_str.startswith(QUESTION):
            currentQuestion = each_line_str[each_line_str.index(':')+1:].rstrip().lstrip()
        elif each_line_str.startswith(QUESTION_ID):
            currentQuestionID = each_line_str[each_line_str.index(':')+1:].rstrip().lstrip()
        elif each_line_str.startswith(DIFFICULTY):
            questionDict[storyID].append((currentQuestionID, currentQuestion))





def initSemanticStructures():
    titleText = open(SEMANTIC_PATH+TITLE_FILE)

    #creating the set of titles
    for each_title in titleText:
        titleSet.add(each_title.rstrip())

    #creating the set of human words
    humanText = open(SEMANTIC_PATH+HUMAN_FILE)

    for each_human in humanText:
        humanSet.add(each_human.rstrip())

    #creating the set of name words
    nameText = open(SEMANTIC_PATH + NAME_FILE)

    for each_name in nameText:
        nameSet.add(each_name.rstrip())

    #creating the set of occupation words
    occText = open(SEMANTIC_PATH + OCCUPATION_FILE)

    for each_occ in occText:
        occupationSet.add(each_occ.rstrip().lower())

    #creating the set of location words
    locationText = open(SEMANTIC_PATH+LOCATION_FILE)

    for each_loc in locationText:
        locationSet.add(each_loc.rstrip())

    #creating the set of month words
    monthText = open(SEMANTIC_PATH+MONTH_FILE)

    for each_month in monthText:
        monthSet.add(each_month.rstrip().lower())

    #creating the set of time words
    timeText = open(SEMANTIC_PATH+TIME_FILE)

    for each_time in timeText:
        timeSet.add(each_time.rstrip().lower())

    #creating the set of stop words
    stopText = open(SEMANTIC_PATH+STOPWORD_FILE)

    for each_word in stopText:
        stopWordSet.add(each_word.rstrip().lower())

    #creating the set of location prep words
    locprepText = open(SEMANTIC_PATH+LOCPREP_FILE)

    for each_word in locprepText:
        locPrepSet.add(each_word.rstrip().lower())

    locnounText = open(SEMANTIC_PATH+LOCNOUN_FILE)

    for each_word in locnounText:
        locNounSet.add(each_word.rstrip().lower())


def who_rules(question, sentence):
    #print sentence
    score = WordMatch(question, sentence)
    sentenceNameFound = False
    questionNameFound = False
    nameReturn = ''
    humanInSentence = ''
    taggedSent = nltk.pos_tag(sentence)
    namedEntSent = nltk.ne_chunk(taggedSent)
    for each in namedEntSent:
        if type(each) is nltk.Tree:
            if each.label() == "PERSON":
                sentenceNameFound = True
                humanInSentence = " ".join([name[0] for name in each.leaves()])


    taggedQues = nltk.pos_tag(question)
    namedEntQues = nltk.ne_chunk(taggedQues)
    for each in namedEntQues:
        if type(each) is nltk.Tree:
            if each.label() == "PERSON":
                questionNameFound = True

    sentence = [ each.lower() for each in sentence]
    sentWordSet = set(sentence)
    sentNamesHuman = sentWordSet.intersection(occupationSet)

    if not questionNameFound and sentenceNameFound:
        score += CONFIDENT

    if not questionNameFound and 'name' in sentWordSet:
        score += GOODCLUE

    if sentenceNameFound or len(sentNamesHuman)>0:
        humanInSentence += " "+ " ".join(list(sentNamesHuman))
        #nameReturn =humanInSentence
        score += GOODCLUE

    # exampleArray = ['Bryce Cannyon is a beautiful place not in INDIA but in Utah']
    # for item in exampleArray:
    #     tokenized = word_tokenize(item)
    #     tagged = nltk.pos_tag(tokenized)
    #     #print tagged
    #
    #     namedEnt = nltk.ne_chunk(tagged)
    #     print namedEnt
    #     # for each in namedEnt:
    #     #     #print type(each) ,'--->',each
    #     #     if type(each) is nltk.Tree:
    #     #         print "tree"
    #     #         if each.label() == "PERSON":
    #     #             print each


    return score, nameReturn

def what_rules(question, sentence):
    score = WordMatch(question, sentence)
    #initially all question and answer rules are not found
    questionRuleFound = [False, False, False, False]
    answerRuleFound = [False, False, False, False]
    headNounTokens = []

    #checking what rules are applicable for question
    for i in range(0, len(question)):
        curr_token = question[i].lower()
        if curr_token in punctuationSet:
            continue
        #rule 1
        if curr_token in monthSet:
            questionRuleFound[0] = True
        #rule 2
        elif curr_token == 'kind':
            questionRuleFound[1] = True
        #rule 3
        elif curr_token == 'name':
            questionRuleFound[2] = True
            #rule 4
            if i < len(question)-1:
                next_token = question[i+1].lower()
                if next_token == 'for' or next_token == 'of':
                    questionRuleFound[3] = True
                    #getting the PP following preposition
                    if i < len(question)-2:
                        headNounTokens = [token.lower() for token in question[i+2:]]

    #print headNounTokens
    #checking what rules are applicable for answer
    for i in range(0, len(sentence)):
        curr_token = sentence[i].lower()
        if curr_token in punctuationSet:
            continue
        if curr_token in timeSet:
            answerRuleFound[0] = True
        if curr_token in set(['call', 'calling', 'called', 'calls', 'from']):
            answerRuleFound[1] = True
        if curr_token in set(['name', 'call', 'known', 'called', 'calls']):
            answerRuleFound[2] = True
        noun_phrases, found = getListofNounPhrases(sentence, set(headNounTokens))
        answerRuleFound[3] = found

    #time to award points!!
    if answerRuleFound[0] and questionRuleFound[0]:
        score += CLUE
    if answerRuleFound[1] and questionRuleFound[1]:
        score += GOODCLUE
    if answerRuleFound[2] and questionRuleFound[2]:
        score += SLAMDUNK
    if answerRuleFound[3] and questionRuleFound[3]:
        score += SLAMDUNK

    return score


def when_rules(question, sentence):

    score = 0
    questionRuleFound = [False, False]
    answerRuleFound = [False, False]
    sentenceL = [each.lower() for each in sentence]
    #print sentence
    sentWordSet = set(sentenceL)

    sentTime = sentWordSet.intersection(timeSet)
    #print "intersection",sentTime

    for i in range(0, len(question)):
        curr_token = question[i].lower()
        if curr_token in punctuationSet:
            continue
        if curr_token == 'the':
            if i < len(question) -1:
                next_token = question[i+1].lower()
                if next_token == 'last':
                    questionRuleFound[0] = True
        if curr_token in set(['start', 'begin']):
            questionRuleFound[1] = True

    for i in range(0, len(sentence)):
        curr_token = sentence[i].lower()
        if curr_token in punctuationSet:
            continue
        if curr_token in set(['first', 'last','since','ago']):
            answerRuleFound[0] = True
        if curr_token in set(['start', 'begin','since','year']):
            answerRuleFound[1] = True

    if len(sentTime)>0 :
        score += GOODCLUE
        score += WordMatch(question, sentence)

    if questionRuleFound[0] and answerRuleFound[0]:
        score += SLAMDUNK

    if questionRuleFound[1] and answerRuleFound[1]:
        score += SLAMDUNK

    return score

def where_rules(question, sentence):
    score = WordMatch(question, sentence)
    locReturn = ''
    sentenceLocFound = False
    sentenceL = [each.lower() for each in sentence]
    sentWordSetLower = set(sentenceL)
    sentLocPrep = sentWordSetLower.intersection(locPrepSet)

    sentWordSet = set(sentence)
    sentLoc = sentWordSet.intersection(locationSet)

    '''taggedSent = nltk.pos_tag(sentence)
    namedSent = nltk.ne_chunk(taggedSent)
    for each in namedSent:
        if type(each) is nltk.Tree:
            if each.label() == "GPE" or each.label() == "LOCATION":
                sentenceLocFound = True'''

    if len(sentLocPrep)>0 :
        score += GOODCLUE

    if len(sentLoc) > 0:
        locReturn = " ".join(list(sentLoc))
        score += CONFIDENT

    return score, locReturn


def why_rules(question, sentScoreList):
    num_sentences = len(sentScoreList)
    num_best = len(sentScoreList)/5
    bestSentences = list(sentScoreList)
    for i in range(0, num_sentences-num_best):
        current_min = min(bestSentences, key=operator.itemgetter(1))
        bestSentences.remove(current_min)

    #resetting scores to zero
    for i in range(0, num_sentences):
        sentScoreList[i] = (sentScoreList[i][0],0,sentScoreList[i][2])

    for i in range(0, num_sentences):
        curr_token = sentScoreList[i][0]
        curr_sent = sentScoreList[i][2]
        score = sentScoreList[i][1]
        current = sentScoreList[i]
        #rule 1
        if current in bestSentences:
            score += CLUE
        #rule 2
        if i < num_sentences-1:
            next_sent = sentScoreList[i+1]
            if next_sent in bestSentences:
                score += CLUE
        #rule 3
        if i>0:
            prev_sent = sentScoreList[i-1]
            if prev_sent in bestSentences:
                score += GOODCLUE
        lower_tokens = [tokens.lower() for tokens in curr_token]
        #rule 4
        if 'want' in lower_tokens:
            score += GOODCLUE

        #rule 5
        if 'so' in lower_tokens or 'because' in lower_tokens:
            score += GOODCLUE
        sentScoreList[i] = (sentScoreList[i][0], score, curr_sent)
    #determining the best score
    best_why = max(sentScoreList, key=operator.itemgetter(1))
    return best_why[2]





def dateline_rules(question):
    questionRuleFound = [False, False,False,False]
    rule2 = [False,False]
    score = 0

    for i in range(0, len(question)):
        curr_token = question[i].lower()

        if curr_token in punctuationSet:
            continue
        if curr_token in set(['happen','happens,happened,happening']):
            questionRuleFound[0] = True
        if curr_token in set(['take', 'took','taking']):
            rule2[0] = True
        if curr_token in 'place':
            rule2[1] = True
        if rule2[0] and rule2[1]:
           questionRuleFound[1] = True
        if curr_token in 'this':
            questionRuleFound[2] = True
        if curr_token in 'story':
            questionRuleFound[3] = True

    if questionRuleFound[0]:
        score += GOODCLUE

    if questionRuleFound[1]:
        score += GOODCLUE

    if questionRuleFound[2]:
        score += SLAMDUNK

    if questionRuleFound[3]:
        score += SLAMDUNK

    return score


def score_sentence(question, sentence, questionType):
    nameReturn = ''
    score = WordMatch(question, sentence)
    if questionType == WHO:
        score, nameReturn = who_rules(question, sentence)
    elif questionType == WHAT:
        score = what_rules(question, sentence)
    elif questionType == WHEN:
        score = when_rules(question, sentence)
    elif questionType == WHERE:
        score = where_rules(question, sentence)
    return score, nameReturn

def refineAnswer(answer):
    answer_tokens = word_tokenize(answer)
    token_collect = []
    for each_token in answer_tokens:
        if each_token.lower() in stopWordSet or each_token.lower() in punctuationSet:
            continue
        token_collect.append(each_token)
    refined_answer = " ".join(token_collect)
    return refined_answer

def removeQuestionWords(answer, questions):
    questionSet = set([each_word.lower() for each_word in questions])
    answerSet = [each_word for each_word in answer if each_word.lower() not in questionSet and each_word not in punctuationSet]
    refined_answer = " ".join(answerSet)
    return refined_answer.rstrip()


def find_answers():
    #print
    for each_story in storyID:
        storyLines = storyDict[each_story][TEXT_KEY]
        storyDates = storyDict[each_story][DATE_KEY]
        questions = questionDict[each_story]
        for each_question in questions:
            #determining question type
            QUESTION_TYPE = DEFAULT
            if 'who' in each_question[1].lower():
                QUESTION_TYPE = WHO
            if 'what' in each_question[1].lower():
                # Start 
                str = each_question[1].lower()
                qwList = str.split()
                i = qwList.index('what')
                nextWord = qwList[i+1]
                taggedQues = nltk.pos_tag(qwList)
                tagOfNextWord = taggedQues[i+1][1]
                if 'NN' in tagOfNextWord:
                    if nextWord in locNounSet:
                        QUESTION_TYPE = WHERE
                    else:
                        QUESTION_TYPE = WHO
                else:
                    QUESTION_TYPE = WHAT
                #End
            # QUESTION_TYPE = WHAT
            if 'when' in each_question[1].lower():
                QUESTION_TYPE = WHEN
            if 'where' in each_question[1].lower():
                QUESTION_TYPE = WHERE
            if 'why' in each_question[1].lower():
                QUESTION_TYPE = WHY
            questionToken = word_tokenize(each_question[1])
            print "QuestionID: " + each_question[0].rstrip()
            maxScore = 0
            maxSentence = ''
            #dateline considered for when and where
            if (QUESTION_TYPE == WHEN or QUESTION_TYPE == WHERE):
                maxScore = dateline_rules(questionToken)
                maxSentence = storyDates

            whyLines = []
            nameReturn = ''
            for each_line in storyLines:
                sentToken = word_tokenize(each_line)
                sentScore, nameReturn = score_sentence(questionToken, sentToken, QUESTION_TYPE)
                if (QUESTION_TYPE == WHY):
                    whyLines.append((sentToken, sentScore, each_line))
                if sentScore >= maxScore:
                    maxScore = sentScore
                    maxSentence = each_line
            if (QUESTION_TYPE == WHY):
                finalAnswer = why_rules(questionToken, whyLines).rstrip()
            elif (not nameReturn == ''):
                finalAnswer = nameReturn
            else:
                finalAnswer = maxSentence.rstrip()
            print "Answer: " + removeQuestionWords(word_tokenize(finalAnswer), questionToken)
            print ""

def produceAnswerKeys(answerKey):
    answerText = open(answerKey)
    for each_line in answerText:
        print each_line.rstrip()
    print ""

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("inputFile")
    args = parser.parse_args()
    inputFile = args.inputFile

    #initializing the semantic data structures
    initSemanticStructures()


    #reading input file
    inputText = open(inputFile)
    firstLine = True
    inputDir = ""
    for each_input in inputText:
        if firstLine:
            inputDir += each_input.rstrip()+"/"
            firstLine = False
            continue
        storyID.append(each_input.rstrip())
        storyFile = inputDir + each_input.rstrip() + STORY_EXTENSION
        questionFile = inputDir + each_input.rstrip() + QUESTION_EXTENSION
        answerFile = inputDir + each_input.rstrip() + ".answers"
        #produceAnswerKeys(answerFile)
        formatStory(storyFile)
        formatQuestionFile(questionFile, each_input.rstrip())

    #question = "Who will the start this?"
    # sentence = "It should Bryce Canyon an year ago in India"
    # where_rules(word_tokenize(question), word_tokenize(sentence))
    # where_rules(word_tokenize(question), word_tokenize(sentence))
    # what_rules(word_tokenize(question), word_tokenize(sentence))

    find_answers()


STORY_EXTENSION = ".story"
QUESTION_EXTENSION = ".questions"
HEADLINE = "HEADLINE:"
DATE = "DATE:"
STORY_ID = "STORYID:"
TEXT = "TEXT:"
QUESTION = "Question:"
QUESTION_ID = "QuestionID:"
DIFFICULTY = "Difficulty:"
SEMANTIC_PATH = "../semantics/"
TITLE_FILE = "titles.txt"
TIME_FILE = "time.txt"
MONTH_FILE = "month.txt"
LOCATION_FILE = "location.txt"
STOPWORD_FILE = "StopWords.txt"
OCCUPATION_FILE = "occupation.txt"
NAME_FILE = "names.txt"
HUMAN_FILE = "human.txt"
LOCPREP_FILE = "LocationPrep.txt"
LOCNOUN_FILE = "LocationNoun.txt"
TEXT_KEY = "text"
DATE_KEY = "date"
HEADLINE_KEY = "headline"
CLUE = 3
GOODCLUE = 4
CONFIDENT = 6
SLAMDUNK = 20
WHO = 1
WHAT = 2
WHEN = 3
WHERE = 4
WHY = 5
DEFAULT = 6


if __name__ == "__main__":
    main()
