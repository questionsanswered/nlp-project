__author__ = 'shreyasingh'
def who_rules(question, sentence):

    score = WordMatch(question, sentence)
    nameSent,sentenceNameFound = getListofNounPhrases(sentence,nameSet)
    nameQues,questionNameFound = getListofNounPhrases(question,nameSet)


    sentence = [ each.lower() for each in sentence]
    #print sentence
    sentWordSet = set(sentence)
    #print sentWordSet
    sentNamesHuman = sentWordSet.intersection(occupationSet)

    if not questionNameFound and sentenceNameFound:
        score += CONFIDENT

    if not questionNameFound and 'name' in sentWordSet:
        score += GOODCLUE

    if sentenceNameFound or len(sentNamesHuman)>0:
        score += GOODCLUE

    return score


def where_rules(question, sentence):
    score = WordMatch(question, sentence)

    sentenceL = [each.lower() for each in sentence]
    sentWordSetLower = set(sentenceL)
    sentLocPrep = sentWordSetLower.intersection(locPrepSet)

    sentWordSet = set(sentence)
    sentLoc = sentWordSet.intersection(locationSet)

    if len(sentLocPrep)>0 :
        score += GOODCLUE

    if len(sentLoc)>0 :
        score += CONFIDENT

    return score



def where_rules(question, sentence):
    score = WordMatch(question, sentence)

    sentenceLocFound = False
    sentenceL = [each.lower() for each in sentence]
    sentWordSetLower = set(sentenceL)
    sentLocPrep = sentWordSetLower.intersection(locPrepSet)

    # sentWordSet = set(sentence)
    # sentLoc = sentWordSet.intersection(locationSet)

    taggedSent = nltk.pos_tag(sentence)
    namedSent = nltk.ne_chunk(taggedSent)
    for each in namedSent:
        if type(each) is nltk.Tree:
            if each.label() == "GPE":
                sentenceLocFound = True

    if len(sentLocPrep)>0 :
        score += GOODCLUE

    if sentenceLocFound:
        score += CONFIDENT

    return score